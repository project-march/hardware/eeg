# Unicorn API

To connect with the headset on Ubuntu via c++ code we need to make use of the Linux Unicorn C API.

We do this by making calls to the "unicorn.h" class.
See "UnicornCAPIAcquisitionExample/main.cpp" for an example.

## Shortcut build
If you whish to build and run the EEG code in a simple command, you can add the following code to your `~/.bashrc` or `~/.march_bash_aliases` file. 

If you did **not** clone the eeg repository in your home folder, change the location of the cd command.

```shell
# EEG build & run shortcut.
alias march_eeg_start='cd ~/eeg/Unicorn\ Linux\ C\ API/x64/ && g++ UnicornCAPIAcquisitionExample/main.cpp -ILib -LLib -lunicorn -pthread -Wl,-R,"$(pwd)/Lib" -o unicorn && ./unicorn'
```


Note: with this command you will only build and run your 'UnicornCAPIAcquisitionExample/main.cpp' file. See the next section on how to change this.

## How to build
To build the file you can use attached Makefile, or do it via terminal commands.
To do it in your terminal type:
```shell
# First cd into this folder. (~/<where you cloned this>/eeg/Unicorn\ Linux\ C\ API/x64).

g++ UnicornCAPIAcquisitionExample/main.cpp -ILib -LLib -lunicorn -pthread -Wl,-R -Wl,"$(pwd)/Lib" -o unicorn
```
* `-ILib` refers to a include directory called './Lib/'.
* `-LLib` refers to a lib directory './Lib/' (a.k.a. LIBRARY_PATH environment variable).
* `-lunicorn` refers to a library file called 'libunicorn.a' or 'libunicorn.so'.
* `-pthread` allows multithreading.
* `-Wl,-R -Wl,"$(pwd)/Lib"` Passes option '-R' with value '"$(pwd)/Lib"' to the linker. 
Equivalent to `-Wl,-R,"$(pwd)/Lib"`, `-Wl,-rpath="$(pwd)/Lib"`, `-Wl,-rpath,"$(pwd)/Lib"`, or
to setting the rpath value of the linker to '"$(pwd)/Lib"'.
* `-o unicorn` Refers to setting name of the resulting executable to 'unicorn'.
  (If not specefied this wil be 'a.out' by default.)




To **run** the application:
```shell
./unicorn
```
To **delete** the application:
```shell
rm unicorn
```
