# Unicorn Data Converter
This program converts the binary data stream acquired from the Unicorn BI EEG headset into
an easier to understand CSV file.

# Installation
## For Linux
```
wget https://gitlab.com/project-march/hardware/eeg/-/raw/main/unicorn_converter/bin/unicorn_converter
chmod 755 unicorn_converter
sudo mv unicorn_converter /usr/bin
```


## From source
Make sure that you have a working installation of Rust installed.
```
git clone https://gitlab.com/project-march/hardware/eeg.git
cd unicorn_converter
cargo install --path .
```

# Usage
This program can be used in two different ways, after or during data acquisition.

## After
```
unicorn_converter [path to data file] > output.csv
```

## During
This mode is interesting, because it can transform the data live. Assuming you are constantly writing to a file called `data.bin`, you can transform it into a CSV file with
```
tail -c +1 -f data.bin | unicorn_converter -
```

# Format
The CSV has 17 columns, each having a floating point value.
| Column | Name                 |
|--------|----------------------|
|    1   | EEG 1                |
|    2   | EEG 2                |
|    3   | EEG 3                |
|    4   | EEG 4                |
|    5   | EEG 5                |
|    6   | EEG 6                |
|    7   | EEG 7                |
|    8   | EEG 8                |
|    9   | Accelerometer X      |
|   10   | Accelerometer Y      |
|   11   | Accelerometer Z      |
|   12   | Gyroscope X          |
|   13   | Gyroscope Y          |
|   14   | Gyroscope Z          |
|   15   | Battery Level        |
|   16   | Counter              |
|   17   | Validation Indicator |

Or, the header in CSV format:
`EEG 1,EEG 2,EEG 3,EEG 4,EEG 5,EEG 6,EEG 7,EEG 8,Accelerometer X,Accelerometer Y,Accelerometer Z,Gyroscope X,Gyroscope Y,Gyroscope Z,Battery Level,Counter,Validation Indicator`

# License
Copyright (C) 2021 Thijs Raymakers

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

