//  Copyright (C) 2021  Thijs Raymakers
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::env;
use std::fs::File;
use std::io::{self, BufRead, BufReader, Read, Stdin, Write};
use std::mem::size_of;
use std::path::Path;

// Each value consists of 4 bytes to make a f32. Each line contains
// 17 values.
const LINE_LENGTH: usize = 17 * size_of::<f32>();

fn main() {
    // Retrieve the initial arguments
    let mut stream = match parse_arguments() {
        Ok(input) => input,
        Err(err) => {
            eprintln!("{}", err);
            std::process::exit(1);
        }
    };

    // Loop until an EOF is found
    loop {
        match convert_stream(stream) {
            Ok(x) => stream = x,
            Err(err) => {
                if err.kind() == io::ErrorKind::BrokenPipe {
                    return;
                }
                eprintln!("{}", err);
                std::process::exit(1);
            }
        };
    }
}

struct BufferedStream {
    stream: Stream,
    buffer: Vec<u8>,
}

impl BufferedStream {
    fn new(stream: Stream) -> Self {
        Self {
            stream,
            buffer: Vec::new(),
        }
    }
}

// I only want to write the logic once, but I also want to
// support both stdin and files. This way, it can be used to converted
// single files, but also to convert a stream with the help of pipes.
// I have implemented the logic for Stream, that delegates the implementation
// for the Read and BufRead traits to either the Stdin implementation or the
// File implementation that is already available in the std libary.
enum Stream {
    StdIn(BufReader<Stdin>),
    File(BufReader<File>),
}

impl Read for Stream {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, std::io::Error> {
        match self {
            Stream::StdIn(x) => x.read(buf),
            Stream::File(x) => x.read(buf),
        }
    }
}

impl BufRead for Stream {
    fn fill_buf(&mut self) -> Result<&[u8], std::io::Error> {
        match self {
            Stream::StdIn(x) => x.fill_buf(),
            Stream::File(x) => x.fill_buf(),
        }
    }
    fn consume(&mut self, s: usize) {
        match self {
            Stream::StdIn(x) => x.consume(s),
            Stream::File(x) => x.consume(s),
        }
    }
}

fn usage(executable_name: &str) -> String {
    format!(
       "\
 ⠀⠀⠀⠀⢀⣤⣴⠶⠿⠿⠿⠿⠶⣦⣄⡀\n\
 ⠀⠀⢀⣄⠈⣉⣤⡶⠿⠂⠘⠿⢶⣤⣉⠁⣠⡀\n\
 ⠀⣰⡟⠁⠺⢋⣡⡀⠸⠛⠛⠃⢀⣌⠛⠗⠈⢻⣆    \x1b[0;1;38;5;9mUnicorn BI to CSV converter\x1b[0m - \x1b[0;3;4;38;5;9mVersion 0.2\x1b[0m\n\
 ⢰⡟⢰⡖⢠⡿⠋⠀⠀⠀⠀⠀⠀⠙⢷⠄⢲⡆⢻⡆\n\
 ⣿⠁⣿⢁⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⡈⣿⠈⣷   \x1b[0;38;5;9mCopyright (C) 2021 Thijs Raymakers\x1b[0m\n\
 ⣿⠀⣿⢸⣏⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⡇⣿⠀⣿   This program comes with ABSOLUTELY NO WARRANTY.\n\
 ⢿⠀⠿⢸⣟⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣻⡇⠿⢠⡿   This program is free software: you can redistribute\n\
 ⠀⠀⠀⠈⣿⡀⠀⠀⠀⠀⠀⠀⢀⡀⢀⣿       it and/or modify it under the terms of the\n\
 ⠀⠀⠀⠀⠹⣷⠘⠻⢷⣦⠰⠾⠟⠃⣿⠃       GNU General Public License as published by the Free\n\
 ⠀⠀⠀⠀⠀⢻⣆⠀⢸⡏⣀⠀⠀⣰⡟        Software Foundation, either version 3 of the License,\n\
 ⠀⠀⠀⠀⠀⠈⢻⣦⠈⠛⠛⢀⣴⠟         or (at your option) any later version.\n\
 ⠀⠀⠀⠀⠀⠀⠀⠙⠷⣶⣶⠿⠋\n\
\n\

        \x1b[0mThis program converts binary data streams acquired with the C API of the\n\
        Unicorn BI into a CSV that can be read by other programs.\n\
        \n\
        Usage: {} filename      for reading from a file\n\
        Usage: {} -             for reading from stdin\n\
        \n\
        The resulting CSV has 17 columns, each having a floating point value.\n
        Col | Name
        ----+-------------------
          1 | EEG 1
          2 | EEG 2
          3 | EEG 3
          4 | EEG 4
          5 | EEG 5
          6 | EEG 6
          7 | EEG 7
          8 | EEG 8
          9 | Accelerometer X
         10 | Accelerometer Y
         11 | Accelerometer Z
         12 | Gyroscope X
         13 | Gyroscope Y
         14 | Gyroscope Z
         15 | Battery Level
         16 | Counter
         17 | Validation Indicator\n\
         \n\
         Or in CSV format:\n\
         EEG 1,EEG 2,EEG 3,EEG 4,EEG 5,EEG 6,EEG 7,EEG 8,Accelerometer X,Accelerometer Y,Accelerometer Z,Gyroscope X,Gyroscope Y,Gyroscope Z,Battery Level,Counter,Validation Indicator\
        ",
        executable_name, executable_name
    )
}

fn parse_arguments() -> Result<BufferedStream, String> {
    let arguments: Vec<String> = env::args().collect();

    if arguments.len() != 2 {
        let command_str = String::from("command");
        let executable_name = arguments.get(0).unwrap_or(&command_str);
        return Err(usage(executable_name));
    }

    Ok(match arguments[1].as_str() {
        "-" => BufferedStream::new(Stream::StdIn(BufReader::new(io::stdin()))),
        path => {
            let file = File::open(Path::new(path)).map_err(|e| e.to_string())?;
            BufferedStream::new(Stream::File(BufReader::new(file)))
        }
    })
}

fn copy_data_into_buffer(mut buf_stream: BufferedStream) -> Result<BufferedStream, std::io::Error> {
    let buffer = buf_stream.stream.fill_buf()?;

    // If the buffer is empty, that means that the stream reached an EOF
    // (from documentation of fill_buf())
    if buffer.is_empty() {
        return Err(io::Error::new(io::ErrorKind::BrokenPipe, "EOF reached"));
    }

    let length = buffer.len();

    // Move elements of the original buffer to the buffer in the buffered stream
    buf_stream.buffer.reserve(length);
    for byte in buffer {
        buf_stream.buffer.push(*byte);
    }

    // Per the documentation of buffered readers, `fill_buf` always needs to be
    // followed by a `consume` that marks the amount of bytes that have been read.
    buf_stream.stream.consume(length);

    Ok(buf_stream)
}

fn convert_stream(buf_stream: BufferedStream) -> Result<BufferedStream, std::io::Error> {
    let mut buf_stream = copy_data_into_buffer(buf_stream)?;

    let line_iter = buf_stream.buffer.chunks_exact(LINE_LENGTH);
    let amount_of_lines = line_iter.len();
    for line in line_iter {
        let mut output = Vec::new();

        // Assuming size_of(f32) is 4, this will always succeed because
        // chunks_exact will guarantee that there are 4 elements for
        // each iteration.
        for value in line.chunks_exact(size_of::<f32>()) {
            let array: [u8; 4] = [value[0], value[1], value[2], value[3]];
            let bits = u32::from_le_bytes(array);
            let float = f32::from_bits(bits);
            write!(&mut output, "{},", float)?;
        }
        // Remove the last trailing , from the string and add a newline.
        output.pop();
        writeln!(&mut output)?;

        // Write the total result to stdout (NOT via println, because that
        // causes problems when chaining the program in a different pipe)
        io::stdout().write_all(&output)?;
    }

    // Remove the processed lines from the buffer, but keep the unprocessed lines.
    let left_to_process = buf_stream.buffer.split_off(amount_of_lines * LINE_LENGTH);
    buf_stream.buffer = left_to_process;

    Ok(buf_stream)
}
